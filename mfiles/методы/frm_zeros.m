function Arr_frm  = frm_zeros( Arr_orig ,sfrm)
size_arr_orig=size(Arr_orig);
Arr_frm=int32(zeros(size_arr_orig(1)+sfrm*2,size_arr_orig(2)+sfrm*2));
for i=sfrm+1:1:size_arr_orig(1)+sfrm
    for j=sfrm+1:1:size_arr_orig(2)+sfrm
      Arr_frm(i,j)=  Arr_orig(i-sfrm,j-sfrm);
    end
end

end

