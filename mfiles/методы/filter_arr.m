function Array= filter_arr ( Arr_orig, Arr_filt,r)
    size_filter=size(Arr_filt);
    n=fix(size_filter(1)/2);
    Array_tmp = frm_zeros( Arr_orig ,n) ;
    size_arr_tmp=size(Array_tmp);
    Array1=zeros( size_arr_tmp(1)-2*n,size_arr_tmp(2)-2*n);
    Array=int32(Array1);
    for i=1:1:size_arr_tmp(1)-2*n
        for j=1:1:size_arr_tmp(2)-2*n
            for k=1:1:size_filter(1)
                for l=1:1:size_filter(1)
            Array(i,j)=Array(i,j)+Array_tmp(i+k-1,j+l-1)*Arr_filt(k,l);
                end
            end
            Array(i,j)=Array(i,j)/r;
        end
    end
    Array=uint8(Array);
end

