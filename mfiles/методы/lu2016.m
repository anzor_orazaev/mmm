load('C:\Program Files\MATLAB\R2017b\bin\mfiles\lena75.mat')
frame = 17;
mask=1;
step=1;
% SP = SP1;
z1=0;
PSNR=zeros();
SSIM=zeros();
P1=zeros();
SP1 = frm_zeros( SP1 ,frame);
P1 = double(frm_zeros( P1 ,frame));

for m=1:1:step
    AA=0;
    z1=z1+1;
    SP=SP1;
        for i=1:1:row
            for j=1:1:col
                if (SP(i+frame,j+frame)~=255) && (SP(i+frame,j+frame)~=0)
                    AA(i,j)=SP(i+frame,j+frame);
                else
                    k=0;
%                     vek1=0;
%                     clear vek1;
                        for p1=-mask:1:mask
                        for p2=-mask:1:mask                             
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                        
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmin=0;
                        xmax=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=p1max*xmax+p1min*xmin;
                        
                       

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for iii=1:1:k
                                  if (abs(vek1(iii)-xmax)<(abs(vek1(iii)-xmin))) && (abs(vek1(iii)-xmax)<(abs(vek1(iii)-xmid)))
                                    cmax=cmax+1;
                                  else
                                      if (abs(vek1(iii)-xmin)<(abs(vek1(iii)-xmax))) && (abs(vek1(iii)-xmin)<(abs(vek1(iii)-xmid)))
                                     cmin=cmin+1;
                                      else
                                                       
                                  cmid=cmid+1;
                                      end 
                                  end
                                  
                             end 
                            
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            
                             P1(i,j)=min(vek1);
                            
                            AA(i,j)=p2max*xmax+p2min*xmin+p2mid*xmid;
                    else
%      2                  
%                         vek1=0;
                        k=0;
                        for p1=-mask-1:1:mask+1
                        for p2=-mask-1:1:mask+1
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%   3                       
%                         vek1=0;
                        k=0;
                        for p1=-mask-2:1:mask+2
                        for p2=-mask-2:1:mask+2
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%            4
%                         vek1=0;
                        k=0;
                        for p1=-mask-3:1:mask+3
                        for p2=-mask-3:1:mask+3
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%                5
%                         vek1=0;
                        k=0;
                        for p1=-mask-4:1:mask+4
                        for p2=-mask-4:1:mask+4
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%           6
%                                     vek1=0;
                        k=0;
                        for p1=-mask-5:1:mask+5
                        for p2=-mask-5:1:mask+5
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);      
                    else
%             7
%                         vek1=0;
                        k=0;
                        for p1=-mask-6:1:mask+6
                        for p2=-mask-6:1:mask+6
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%                         8
%                                                 vek1=0;
                        k=0;
                        for p1=-mask-7:1:mask+7
                        for p2=-mask-7:1:mask+7
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%                         9
%                         vek1=0;
                        k=0;
                        for p1=-mask-8:1:mask+8
                        for p2=-mask-8:1:mask+8
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%                         10
%                         vek1=0;
                        k=0;
                        for p1=-mask-9:1:mask+9
                        for p2=-mask-9:1:mask+9
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%                         11
%                         vek1=0;
                        k=0;
                        for p1=-mask-10:1:mask+10
                        for p2=-mask-10:1:mask+10
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%                         12
%                         vek1=0;
                        k=0;
                        for p1=-mask-11:1:mask+11
                        for p2=-mask-11:1:mask+11
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%                         13
%                         vek1=0;
                        k=0;
                        for p1=-mask-12:1:mask+12
                        for p2=-mask-12:1:mask+12
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%                         14
%                         vek1=0;
                        k=0;
                        for p1=-mask-13:1:mask+13
                        for p2=-mask-13:1:mask+13
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else

%                         15
%                         vek1=0;
                        k=0;
                        for p1=-mask-14:1:mask+14
                        for p2=-mask-14:1:mask+14
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                    else
%                         16
%                         vek1=0;
                        k=0;
                        for p1=-mask-15:1:mask+15
                        for p2=-mask-15:1:mask+15
                                if (SP(i+frame+p1,j+frame+p2)~=255) && (SP(i+frame+p1,j+frame+p2)~=0)
                                    k=k+1;
                                    vek1(k)=SP(i+frame+p1,j+frame+p2);
                                end
                        end
                        end
                    if (k~=0)
                        p1min=0;
                        p1max=0;
                        xmid=0;
                        kmin=0;
                        kmax=0;
                        xmin=min(vek1);
                        xmax=max(vek1);
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<=(abs(vek1(ii)-xmin)))
                                    kmax=kmax+1;
                                  else kmin=kmin+1;
                                  end  
                            end
                        p1max=kmax/k;
                        p1min=kmin/k;
                        xmid=round(p1max*xmax+p1min*xmin);

                        p2max=0;
                        p2mid=0;
                        p2min=0;
                        cmin=0;
                        cmax=0;
                        cmid=0;
                            for ii=1:1:k
                                  if (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmin))) && (abs(vek1(ii)-xmax)<(abs(vek1(ii)-xmid)))
                                    cmax=cmax+1;
                                  end  
                                  if (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmid)<=(abs(vek1(ii)-xmin)))
                                    cmid=cmid+1;
                                  end 
                                  if (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmax))) && (abs(vek1(ii)-xmin)<(abs(vek1(ii)-xmid)))
                                    cmin=cmin+1;
                                  end 
                            end
                            
                            p2max=cmax/k;
                            p2min=cmin/k;
                            p2mid=cmid/k;
                            AA(i,j)=round(p2max*xmax+p2min*xmin+p2mid*xmid);
                            
                    end
                    end
                    end
                    end
                    end
                    end
                    end
                    end
                    end
                    end
                    end
                    end
                    end
                    end
                    end                 
                    end
                end
            vek1=0;
        end
        end
        SP = AA;
        SP = frm_zeros( SP ,frame);
        AAA=uint8(AA);
    PSNR(z1)=psnr(AAA,f2)
    SSIM(z1)=ssim(AAA,f2)
end


% AAA=uint8(AA);
% PSNR=psnr(AAA,f2)
% clear
% load('mo99.mat')
% PSNR2=psnr(SP1,f2)
% figure; imshow(AAA)
% figure; imshow(SP1)